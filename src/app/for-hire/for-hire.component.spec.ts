import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ForHireComponent } from './for-hire.component';

describe('ForHireComponent', () => {
  let component: ForHireComponent;
  let fixture: ComponentFixture<ForHireComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ForHireComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ForHireComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
