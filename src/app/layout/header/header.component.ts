import { IStates } from './../../share/models/IStates';
import { ITp } from './../../share/models/ITp';
import { HouseService } from './../../share/services/house.service';
import { Component, OnInit } from '@angular/core';
import { FindHouseStates} from './../../share/states/Findhouse.states';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  Tps: ITp[] = [];
  NameTp: String[];
  Quans: IStates[] = [];
  selected: String;
  selectedQ: String;
  constructor(public houseservice: HouseService, private houseStates: FindHouseStates) { }

  ngOnInit() {
    this.houseservice.getCity().subscribe(Tp => {
      this.Tps = Tp;
      this.selected = this.Tps[0].code;
      this.Quans = this.Tps[0].states;
      console.log(this.Quans)
    });
  }
  selectTp(selected: ITp) {
    const currentCity = this.Tps.find(t => t.code === selected);
    this.Quans = currentCity.states;
  }
  search() {
    console.log (this.selectedQ);
     this.houseStates.find(this.selectedQ as String);
  }
}
