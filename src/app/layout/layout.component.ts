import { HouseService } from './../share/services/house.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.css']
})
export class LayoutComponent implements OnInit {
  data: String[];
  constructor(public houseservice: HouseService) { }

  ngOnInit() {
  }
}
