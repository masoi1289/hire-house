import { IHouse } from './../share/models/IHouse';
import { HouseService } from './../share/services/house.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-hire-house',
  templateUrl: './hire-house.component.html',
  styleUrls: ['./hire-house.component.css']
})
export class HireHouseComponent implements OnInit {
  houses: IHouse[]= [];
  constructor(public houseservice: HouseService) { }

  ngOnInit() {
    this.houseservice.getHouses().subscribe(houses => {
      this.houses = houses;
    });
  }

}
