import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HireHouseComponent } from './hire-house.component';

describe('HireHouseComponent', () => {
  let component: HireHouseComponent;
  let fixture: ComponentFixture<HireHouseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HireHouseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HireHouseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
