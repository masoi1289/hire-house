import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  template: `<app-layout style="display: flex; flex-direction: column;"></app-layout>
  <router-outlet></router-outlet>`,
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';
}
