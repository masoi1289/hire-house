import { HouseService } from './../share/services/house.service';
import { IHouse } from './../share/models/IHouse';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-house',
  templateUrl: './house.component.html',
  styleUrls: ['./house.component.css']
})
export class HouseComponent implements OnInit {
  // house: IHouse = [];
   @Input() house: IHouse;
  @Output()  selectHouse = new EventEmitter<IHouse>();
  @Output()  cooihouse = new EventEmitter<IHouse>();
  constructor(private houseservice: HouseService) { }

  ngOnInit() {
    // this.houseservice.getHouses().subscribe(house => {
    //   this.house = house;
    // });
  }
  pickhouse(house: IHouse) {
    console.log(house.id);
  }
  // coihouse(house: IHouse) {
  //   this.cooihouse.emit(house);
  //   console.log(house);
  // }
  coihouse(house: IHouse) {
    this.cooihouse.emit(house);
  }
  }


