import { FindHouseStates } from './../share/states/Findhouse.states';
import { HouseService } from './../share/services/house.service';
import { IHouse } from './../share/models/IHouse';
import { HouseDetailComponent } from './../house-detail/house-detail.component';
import { Component, OnInit, Input } from '@angular/core';
import { AgmCoreModule } from '@agm/core';
import { ElementRef, NgZone, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { } from 'googlemaps';
import { MapsAPILoader } from '@agm/core';
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  title: String = 'My first AGM project';
  lat: Number;
  lng: Number;
  latlocal: Number;
  lnglocal: Number;
  zoom: Number = 13;
  selectedid: Number;
  @ViewChild('search')
  public searchElementRef: ElementRef;


  houses: IHouse[] = [];
  constructor(public houseservice: HouseService, private housetState: FindHouseStates, private mapsAPILoader: MapsAPILoader,
    private ngZone: NgZone) { }

  ngOnInit() {
    this.houseservice.getHouses().subscribe(houses => {
      this.houses = houses;
    });
    this.housetState.house.subscribe(houses => {
      this.houses = houses;
    });
    this.housetState.getAll();
    this.getUserLocation();
    console.log(this.latlocal);
    this.setCurrentPosition();
    this.mapsAPILoader.load().then(() => {
      const autocomplete = new google.maps.places.Autocomplete(this.searchElementRef.nativeElement, {
        types: ['address']
      });
      autocomplete.addListener('place_changed', () => {
        this.ngZone.run(() => {
          const place: google.maps.places.PlaceResult = autocomplete.getPlace();

          if (place.geometry === undefined || place.geometry === null) {
            return;
          }
          this.lat = place.geometry.location.lat();
          this.lng = place.geometry.location.lng();
          this.zoom = 12;
        });
      });
    });




  }






  coihouse(houses: IHouse) {
    this.lat = houses.lat;
    this.lng = houses.lng;
    console.log(houses.lat);
  }
  private getUserLocation() {
    /// locate the user
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(position => {
        console.log(position);
        this.latlocal = position.coords.latitude;
        this.lnglocal = position.coords.longitude;
      });
    }
  }
  private setCurrentPosition() {
    if ('geolocation' in navigator) {
      navigator.geolocation.getCurrentPosition((position) => {
        this.lat = position.coords.latitude;
        this.lng = position.coords.longitude;
        this.zoom = 12;
      });
    }
  }
}
