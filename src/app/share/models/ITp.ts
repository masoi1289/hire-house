import { IStates } from './IStates';
export interface ITp {
    name?: String;
    code?: String;
    states?: Array<IStates>;
}
