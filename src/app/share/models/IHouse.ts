export interface IHouse {
    id?: number;
    name?: String;
    type?: String;
    city?: String;
    district?: String;
    street?: String;
    length?: number;
    width?: number;
    price?: number;
    img?: String;
    lat?: number;
    lng?: number;
    spn?: number;
    svs?: number;
    p1?: String;
}

