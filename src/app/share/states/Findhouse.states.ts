import { HouseService } from './../services/house.service';
import { IHouse } from './../models/IHouse';
import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

@Injectable()
export class FindHouseStates {
    private _houses: BehaviorSubject<IHouse[]> = new BehaviorSubject(new Array());
    get house() {
        return this._houses.asObservable();
    }
    constructor(private houseService: HouseService) { }

    getAll() {
        const houses = this.houseService.getHouses().subscribe(data => {
            this._houses.next(data);
        });
    }

    find(keyword: String) {
        this.houseService.searchHouses(keyword).subscribe(results => {
            this._houses.next(results);
        });
    }

}