import { ITp } from './../models/ITp';
import { IHouse } from './../models/IHouse';
import { Injectable, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/catch';



@Injectable()
export class HouseService {
  public Tp: ITp[] = [];
  public houses: IHouse[] = [];
  constructor(public http: Http) {
    this.getHouses().subscribe(houses => {
      this.houses = houses;
    });
    // this.http.get('assets/data1.json').subscribe(res => {
    //   console.log(res.json());
    //   this.data = res.json();
    //   console.log(this.data.Tp[0].states);
    // });
    this.getCity().subscribe(Tp => {
      this.Tp = Tp;
      // console.log(this.Tp[0].name);
    });
  }

  getHouses(): Observable<IHouse[]> {
    // const uri = 'https://api.myjson.com/bins/nnkx7';http.get(uri)
    const uri = 'assets/data.json';
    return this.http.get(uri).map(res => {
      return res.json() as IHouse[];
    });
  }
  getCity(): Observable<ITp[]> {
    return this.http.get('assets/data1.json').map(res => {
      // console.log(res.json());
      // return this.data = res.json();
      return res.json()as ITp[];
    });
     // console.log(this.data.Tp[0].name);
  }
  findhousebyId(id: number): Observable<IHouse> {
    return this.getHouses().map(houses => {
      return houses.find(house => {
        return house.id === id;
      });
    });
  }
  searchHouses(quan: String) {
    return this.getHouses().map(houses => {
      return houses.filter(house => {
        return house.district.toLowerCase().includes(quan.toLowerCase());
      });
    });
  }
  
}
