import { HouseService } from './../share/services/house.service';
import { IHouse } from './../share/models/IHouse';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-house-detail',
  templateUrl: './house-detail.component.html',
  styleUrls: ['./house-detail.component.css']
})
export class HouseDetailComponent implements OnInit {
  house: IHouse = {};
  constructor(private route: ActivatedRoute, private houseservice: HouseService) { }

  ngOnInit() {
    this.route.params.subscribe(params => {
      const id = parseInt(params['id'] as string, 0);
      this.houseservice.findhousebyId(id).subscribe(house => {
        this.house = house;
      });
    });
  }

}
