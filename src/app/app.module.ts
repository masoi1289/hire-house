import { HouseService } from './share/services/house.service';
import { HeaderComponent } from './layout/header/header.component';
import { LayoutComponent } from './layout/layout.component';
import { HouseDetailComponent } from './house-detail/house-detail.component';
import { HouseComponent } from './house/house.component';
import { ForHireComponent } from './for-hire/for-hire.component';
import { HireHouseComponent } from './hire-house/hire-house.component';
import { HomeComponent } from './home/home.component';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule} from '@angular/router';
import { HttpModule } from '@angular/http';
import { AgmCoreModule } from '@agm/core';
import { FormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import {FindHouseStates} from './share/states/Findhouse.states';
import { ElementRef, NgZone, OnInit, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { } from 'googlemaps';
import { MapsAPILoader } from '@agm/core';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    ForHireComponent,
    HireHouseComponent,
    HouseComponent,
    HouseDetailComponent,
    LayoutComponent,
    HeaderComponent,
  ],
  imports: [
    BrowserModule,
    HttpModule ,
    FormsModule,
    RouterModule.forRoot([
      {path: 'home', component : HomeComponent},
      {path : 'house/:id', component : HouseDetailComponent },
      {path: 'hire', component : HireHouseComponent},
      {path: 'for-hire', component : ForHireComponent},
      {path : '**', component: HomeComponent},
      {path : '', component : HomeComponent }
    ]),
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyB0c3onWCNTgztTwLF6E0K9bxuK65cHSkI',
      libraries: ['places']
    })
  ],
  providers: [HouseService, FindHouseStates],
  bootstrap: [AppComponent]
})
export class AppModule { }
